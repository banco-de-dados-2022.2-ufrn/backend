--
-- PostgreSQL database dump
--

-- Dumped from database version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Categoria; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Categoria" (
    id integer NOT NULL,
    nome character varying(255) NOT NULL
);


ALTER TABLE public."Categoria" OWNER TO matthieu;

--
-- Name: Categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: matthieu
--

CREATE SEQUENCE public."Categoria_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Categoria_id_seq" OWNER TO matthieu;

--
-- Name: Categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthieu
--

ALTER SEQUENCE public."Categoria_id_seq" OWNED BY public."Categoria".id;


--
-- Name: Consulta; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Consulta" (
    id integer NOT NULL,
    data_horario_consulta timestamp(3) without time zone NOT NULL,
    descricao character varying(255) NOT NULL,
    peso double precision NOT NULL,
    remedio_id integer NOT NULL,
    exames_solicitados_id integer NOT NULL,
    altura double precision NOT NULL,
    temperatura double precision NOT NULL,
    idade integer NOT NULL,
    pressao_arterial character varying(255) NOT NULL,
    medico_crm character varying(6) NOT NULL,
    enfermeiro_coren character varying(6) NOT NULL,
    paciente_cpf character varying(11) NOT NULL
);


ALTER TABLE public."Consulta" OWNER TO matthieu;

--
-- Name: Consulta_id_seq; Type: SEQUENCE; Schema: public; Owner: matthieu
--

CREATE SEQUENCE public."Consulta_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Consulta_id_seq" OWNER TO matthieu;

--
-- Name: Consulta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthieu
--

ALTER SEQUENCE public."Consulta_id_seq" OWNED BY public."Consulta".id;


--
-- Name: Enfermeiro; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Enfermeiro" (
    coren character varying(6) NOT NULL,
    nome character varying(255) NOT NULL
);


ALTER TABLE public."Enfermeiro" OWNER TO matthieu;

--
-- Name: Exame; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Exame" (
    id integer NOT NULL,
    nome character varying(255) NOT NULL,
    categoria_id integer NOT NULL
);


ALTER TABLE public."Exame" OWNER TO matthieu;

--
-- Name: Exame_id_seq; Type: SEQUENCE; Schema: public; Owner: matthieu
--

CREATE SEQUENCE public."Exame_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Exame_id_seq" OWNER TO matthieu;

--
-- Name: Exame_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthieu
--

ALTER SEQUENCE public."Exame_id_seq" OWNED BY public."Exame".id;


--
-- Name: Medico; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Medico" (
    crm character varying(6) NOT NULL,
    nome character varying(255) NOT NULL
);


ALTER TABLE public."Medico" OWNER TO matthieu;

--
-- Name: Paciente; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Paciente" (
    nome character varying(255) NOT NULL,
    cpf character varying(11) NOT NULL,
    inadimplente boolean DEFAULT false NOT NULL,
    diabetico boolean DEFAULT false NOT NULL,
    tipo_diabetes integer NOT NULL,
    hipertenso boolean DEFAULT false NOT NULL,
    doenca_cronica boolean DEFAULT false NOT NULL
);


ALTER TABLE public."Paciente" OWNER TO matthieu;

--
-- Name: Remedio; Type: TABLE; Schema: public; Owner: matthieu
--

CREATE TABLE public."Remedio" (
    id integer NOT NULL,
    nome character varying(255) NOT NULL,
    dosagem_maxima integer NOT NULL,
    dosagem_minima integer NOT NULL
);


ALTER TABLE public."Remedio" OWNER TO matthieu;

--
-- Name: Remedio_id_seq; Type: SEQUENCE; Schema: public; Owner: matthieu
--

CREATE SEQUENCE public."Remedio_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Remedio_id_seq" OWNER TO matthieu;

--
-- Name: Remedio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: matthieu
--

ALTER SEQUENCE public."Remedio_id_seq" OWNED BY public."Remedio".id;


--
-- Name: Categoria id; Type: DEFAULT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Categoria" ALTER COLUMN id SET DEFAULT nextval('public."Categoria_id_seq"'::regclass);


--
-- Name: Consulta id; Type: DEFAULT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta" ALTER COLUMN id SET DEFAULT nextval('public."Consulta_id_seq"'::regclass);


--
-- Name: Exame id; Type: DEFAULT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Exame" ALTER COLUMN id SET DEFAULT nextval('public."Exame_id_seq"'::regclass);


--
-- Name: Remedio id; Type: DEFAULT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Remedio" ALTER COLUMN id SET DEFAULT nextval('public."Remedio_id_seq"'::regclass);


--
-- Data for Name: Categoria; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Categoria" (id, nome) FROM stdin;
2	Soro
3	Fezes
4	Sangue
5	Sangue
6	Soro
7	Hemograma
10	Sangue
11	Hemograma
13	Sangue
16	Soro
\.


--
-- Data for Name: Consulta; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Consulta" (id, data_horario_consulta, descricao, peso, remedio_id, exames_solicitados_id, altura, temperatura, idade, pressao_arterial, medico_crm, enfermeiro_coren, paciente_cpf) FROM stdin;
1	2022-12-22 03:16:18.489	Dolorum veniam laboriosam quisquam. Beatae tempora repellendus. Nemo autem similique occaecati temporibus nostrum. Animi amet beatae assumenda ab accusamus culpa beatae ex alias. Nobis incidunt accusamus.	148.4	2	2	1.36	36.9	73	15.28/9.73	538331	907669	38214766704
2	2022-12-21 20:38:42.268	Quidem voluptatum eaque. Voluptate maiores repudiandae nostrum ipsam at. Fuga architecto reiciendis incidunt fuga perspiciatis doloribus similique.	106.74	3	3	2.16	35.33	14	8.46/6.88	577650	942593	28108337715
3	2022-12-22 12:48:50.136	Alias laudantium est nemo labore quaerat. Beatae quo ut consectetur veniam voluptatibus nesciunt quidem. Dolores optio provident nobis. Error ullam fugiat alias consequatur veniam esse sapiente temporibus quam. Distinctio numquam voluptatum quos.	77.99	4	4	1.93	39.86	20	10.29/5.81	382569	170744	75605549654
4	2022-12-22 14:33:22.918	Id voluptatum nobis accusamus repellendus accusantium. Natus delectus voluptatibus deleniti nostrum corrupti quod voluptate architecto. Ipsam possimus labore quaerat saepe molestias ea illum odio dolorem.	117.14	5	5	1.13	39.55	53	13.99/6.44	860037	391812	79850782101
5	2022-12-22 10:13:22.811	Tempore reiciendis non porro sequi voluptatibus delectus facilis. Sequi harum laudantium quae natus. Et voluptatum libero quibusdam quibusdam. Expedita totam voluptate vitae distinctio recusandae nihil.	124.95	6	6	1.78	39.87	12	15.26/10.03	888572	190571	64305404678
6	2022-12-21 23:17:36.54	Ipsam maiores cupiditate sit magnam nisi. Expedita voluptatibus aliquam nam recusandae perferendis aliquid. Consequatur alias aliquam.	110.97	7	7	2.17	39.48	13	15.29/10.93	175460	822612	52946866194
7	2022-12-22 17:07:27.453	Placeat dolorum ducimus consequuntur unde voluptates voluptatem. Nemo officiis iure. Iusto hic et illum deleniti fuga.	125.71	10	10	1.49	37.19	82	14.96/6.69	229298	106538	75605414001
8	2022-12-22 10:57:47.31	Praesentium ab dolorum sed maiores aspernatur dolorem quia ratione. Saepe culpa itaque. Beatae nulla consectetur impedit in voluptas perferendis expedita magni.	106.61	11	11	2.3	36.99	95	14.07/10.75	680130	821018	77665214313
9	2022-12-21 20:55:50.057	Corporis sit eum ad. Enim natus at at consectetur. Esse voluptatibus dolorum repellat voluptatibus. Nulla laudantium autem sint asperiores.	34.75	13	13	2.13	39.57	93	10.58/6.22	339779	866663	57101689512
10	2022-12-22 11:39:30.574	Eos molestiae consequatur hic. Velit dolorem corporis voluptate iure fugiat. Modi soluta in sequi ullam. Asperiores saepe odio facere eum iste.	86.16	16	16	1.55	39.2	52	15/8.3	349432	880843	74970536508
\.


--
-- Data for Name: Enfermeiro; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Enfermeiro" (coren, nome) FROM stdin;
907669	Mariana Nogueira Filho
942593	Eloá Oliveira
170744	Danilo Reis
391812	Warley Moraes
190571	Maria Júlia Oliveira
822612	Júlia Carvalho
106538	Maria Luiza Oliveira
821018	Elísio Moraes
866663	Eloá Saraiva
880843	Yuri Albuquerque
\.


--
-- Data for Name: Exame; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Exame" (id, nome, categoria_id) FROM stdin;
2	Vitamina B12	2
3	Hemograma	3
4	TSH	4
5	Ferro	5
6	Creatinina	6
7	T3	7
10	Ferro	10
11	TTPA	11
13	Creatinina	13
16	Ferro	16
\.


--
-- Data for Name: Medico; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Medico" (crm, nome) FROM stdin;
538331	Víctor Albuquerque
577650	Karla Barros
382569	Washington Santos
860037	Breno Braga
888572	João Lucas Barros
175460	Aline Saraiva
229298	João Macedo
680130	Nicolas Martins
339779	Felícia Xavier Filho
349432	Emanuel Franco
\.


--
-- Data for Name: Paciente; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Paciente" (nome, cpf, inadimplente, diabetico, tipo_diabetes, hipertenso, doenca_cronica) FROM stdin;
Deneval Nogueira	38214766704	t	f	2	t	t
Márcia Saraiva	28108337715	f	f	1	t	t
Fabiano Macedo	75605549654	t	t	2	f	t
Warley Saraiva	79850782101	t	f	2	t	f
Dra. Mariana Batista	64305404678	t	t	1	f	t
Antonella Oliveira	52946866194	t	t	2	t	f
Ana Laura Reis	75605414001	t	f	2	f	f
Ofélia Silva	77665214313	f	f	2	t	f
Carlos Barros Neto	57101689512	f	f	2	t	f
Paulo Nogueira Neto	74970536508	f	t	1	t	t
\.


--
-- Data for Name: Remedio; Type: TABLE DATA; Schema: public; Owner: matthieu
--

COPY public."Remedio" (id, nome, dosagem_maxima, dosagem_minima) FROM stdin;
2	Corticoide	4543	2921
3	Alprazolam	2843	3874
4	Diclofenaco	1830	3473
5	Alprazolam	7065	6676
6	Aspirina	1845	8361
7	Prednisona	5854	8396
10	Nimesulida	2184	4531
11	Dipirona Sódica	2120	4677
13	Ibuprofeno	2944	9031
16	Aspirina	2287	6309
\.


--
-- Name: Categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthieu
--

SELECT pg_catalog.setval('public."Categoria_id_seq"', 16, true);


--
-- Name: Consulta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthieu
--

SELECT pg_catalog.setval('public."Consulta_id_seq"', 10, true);


--
-- Name: Exame_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthieu
--

SELECT pg_catalog.setval('public."Exame_id_seq"', 16, true);


--
-- Name: Remedio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: matthieu
--

SELECT pg_catalog.setval('public."Remedio_id_seq"', 16, true);


--
-- Name: Categoria Categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Categoria"
    ADD CONSTRAINT "Categoria_pkey" PRIMARY KEY (id);


--
-- Name: Consulta Consulta_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_pkey" PRIMARY KEY (id);


--
-- Name: Enfermeiro Enfermeiro_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Enfermeiro"
    ADD CONSTRAINT "Enfermeiro_pkey" PRIMARY KEY (coren);


--
-- Name: Exame Exame_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Exame"
    ADD CONSTRAINT "Exame_pkey" PRIMARY KEY (id);


--
-- Name: Medico Medico_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Medico"
    ADD CONSTRAINT "Medico_pkey" PRIMARY KEY (crm);


--
-- Name: Paciente Paciente_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Paciente"
    ADD CONSTRAINT "Paciente_pkey" PRIMARY KEY (cpf);


--
-- Name: Remedio Remedio_pkey; Type: CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Remedio"
    ADD CONSTRAINT "Remedio_pkey" PRIMARY KEY (id);


--
-- Name: Consulta Consulta_enfermeiro_coren_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_enfermeiro_coren_fkey" FOREIGN KEY (enfermeiro_coren) REFERENCES public."Enfermeiro"(coren) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Consulta Consulta_exames_solicitados_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_exames_solicitados_id_fkey" FOREIGN KEY (exames_solicitados_id) REFERENCES public."Exame"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Consulta Consulta_medico_crm_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_medico_crm_fkey" FOREIGN KEY (medico_crm) REFERENCES public."Medico"(crm) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Consulta Consulta_paciente_cpf_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_paciente_cpf_fkey" FOREIGN KEY (paciente_cpf) REFERENCES public."Paciente"(cpf) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Consulta Consulta_remedio_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Consulta"
    ADD CONSTRAINT "Consulta_remedio_id_fkey" FOREIGN KEY (remedio_id) REFERENCES public."Remedio"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Exame Exame_categoria_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: matthieu
--

ALTER TABLE ONLY public."Exame"
    ADD CONSTRAINT "Exame_categoria_id_fkey" FOREIGN KEY (categoria_id) REFERENCES public."Categoria"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

