import { PrismaClient } from '@prisma/client';
import { faker } from '@faker-js/faker/locale/pt_BR';

const medicamentos = [
  'Dipirona',
  'Paracetamol',
  'Ibuprofeno',
  'Dorflex',
  'Doril',
  'Dipirona Sódica',
  'Aspirina',
  'Diclofenaco',
  'Corticoide',
  'Dexametasona',
  'Cortisona',
  'Prednisona',
  'Nimesulida',
  'Bromazepam',
  'Valium',
  'Alprazolam',
  'Lorazepam',
];

const exames = [
  'Hemograma',
  'Glicemia',
  'Colesterol',
  'Triglicérides',
  'Creatinina',
  'Ureia',
  'TGO',
  'TGP',
  'TTP',
  'TTPA',
  'T3',
  'T4',
  'TSH',
  'Gama GT',
  'Ferro',
  'Ferritina',
  'Vitamina B12',
];

const categoriaExames = ['Sangue', 'Urina', 'Fezes', 'Soro', 'Hemograma'];

const prisma = new PrismaClient();

async function main() {
  await prisma.consulta.create({
    data: {
      altura: Number(faker.datatype.float({ min: 1.1, max: 2.3 })),
      peso: Number(
        faker.datatype.float({
          min: 30,
          max: 150,
        })
      ),
      pressao_arterial: `${faker.datatype.float({
        min: 8,
        max: 16,
      })}/${faker.datatype.float({
        min: 4,
        max: 12,
      })}`,
      data_horario_consulta: faker.date.recent(),
      medico: {
        create: {
          nome: faker.name.fullName(),
          crm: faker.random.numeric(6),
        },
      },
      enfermeiro: {
        create: {
          nome: faker.name.fullName(),
          coren: faker.random.numeric(6),
        },
      },
      descricao: faker.lorem.paragraph(),
      exames_solicitados: {
        create: {
          nome: faker.helpers.arrayElement(exames),
          categoria: {
            create: {
              nome: faker.helpers.arrayElement(categoriaExames),
            },
          },
        },
      },
      idade: Number(faker.random.numeric(2)),
      remedio: {
        create: {
          nome: faker.helpers.arrayElement(medicamentos),
          dosagem_maxima: Number(faker.random.numeric(4)),
          dosagem_minima: Number(faker.random.numeric(4)),
        },
      },
      paciente: {
        create: {
          nome: faker.name.fullName(),
          cpf: faker.random.numeric(11),
          tipo_diabetes: faker.helpers.arrayElement([1, 2]),
          diabetico: faker.datatype.boolean(),
          hipertenso: faker.datatype.boolean(),
          doenca_cronica: faker.datatype.boolean(),
          inadimplente: faker.datatype.boolean(),
        },
      },
      temperatura: Number(faker.datatype.float({ min: 35, max: 40 })),
    },
  });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async e => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
