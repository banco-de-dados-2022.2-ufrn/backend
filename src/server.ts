import { PrismaClient } from '@prisma/client';
import express from 'express';

const app = express();

const prisma = new PrismaClient();

app.get('/consultas', async (request, response) => {
  const consultas = await prisma.consulta.findMany();
  return response.json(consultas);
});

app.listen(5500, () => console.log('Server is running on port 5500'));
