-- CreateTable

CREATE TABLE
    "Remedio" (
        "id" SERIAL NOT NULL,
        "nome" VARCHAR(255) NOT NULL,
        "dosagem_maxima" INTEGER NOT NULL,
        "dosagem_minima" INTEGER NOT NULL,
        CONSTRAINT "Remedio_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Categoria" (
        "id" SERIAL NOT NULL,
        "nome" VARCHAR(255) NOT NULL,
        CONSTRAINT "Categoria_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Medico" (
        "crm" VARCHAR(6) NOT NULL,
        "nome" VARCHAR(255) NOT NULL,
        CONSTRAINT "Medico_pkey" PRIMARY KEY ("crm")
    );

-- CreateTable

CREATE TABLE
    "Enfermeiro" (
        "coren" VARCHAR(6) NOT NULL,
        "nome" VARCHAR(255) NOT NULL,
        CONSTRAINT "Enfermeiro_pkey" PRIMARY KEY ("coren")
    );

-- CreateTable

CREATE TABLE
    "Exame" (
        "id" SERIAL NOT NULL,
        "nome" VARCHAR(255) NOT NULL,
        "categoria_id" INTEGER NOT NULL,
        CONSTRAINT "Exame_pkey" PRIMARY KEY ("id")
    );

-- CreateTable

CREATE TABLE
    "Paciente" (
        "nome" VARCHAR(255) NOT NULL,
        "cpf" VARCHAR(11) NOT NULL,
        "inadimplente" BOOLEAN NOT NULL DEFAULT false,
        "diabetico" BOOLEAN NOT NULL DEFAULT false,
        "tipo_diabetes" INTEGER NOT NULL,
        "hipertenso" BOOLEAN NOT NULL DEFAULT false,
        "doenca_cronica" BOOLEAN NOT NULL DEFAULT false,
        CONSTRAINT "Paciente_pkey" PRIMARY KEY ("cpf")
    );

-- CreateTable

CREATE TABLE
    "Consulta" (
        "id" SERIAL NOT NULL,
        "data_horario_consulta" TIMESTAMP(3) NOT NULL,
        "descricao" VARCHAR(255) NOT NULL,
        "peso" DOUBLE PRECISION NOT NULL,
        "remedio_id" INTEGER NOT NULL,
        "exames_solicitados_id" INTEGER NOT NULL,
        "altura" DOUBLE PRECISION NOT NULL,
        "temperatura" DOUBLE PRECISION NOT NULL,
        "idade" INTEGER NOT NULL,
        "pressao_arterial" VARCHAR(255) NOT NULL,
        "medico_crm" VARCHAR(6) NOT NULL,
        "enfermeiro_coren" VARCHAR(6) NOT NULL,
        "paciente_cpf" VARCHAR(11) NOT NULL,
        CONSTRAINT "Consulta_pkey" PRIMARY KEY ("id")
    );

-- AddForeignKey

ALTER TABLE "Exame"
ADD
    CONSTRAINT "Exame_categoria_id_fkey" FOREIGN KEY ("categoria_id") REFERENCES "Categoria"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Consulta"
ADD
    CONSTRAINT "Consulta_remedio_id_fkey" FOREIGN KEY ("remedio_id") REFERENCES "Remedio"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Consulta"
ADD
    CONSTRAINT "Consulta_exames_solicitados_id_fkey" FOREIGN KEY ("exames_solicitados_id") REFERENCES "Exame"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Consulta"
ADD
    CONSTRAINT "Consulta_medico_crm_fkey" FOREIGN KEY ("medico_crm") REFERENCES "Medico"("crm") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Consulta"
ADD
    CONSTRAINT "Consulta_enfermeiro_coren_fkey" FOREIGN KEY ("enfermeiro_coren") REFERENCES "Enfermeiro"("coren") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey

ALTER TABLE "Consulta"
ADD
    CONSTRAINT "Consulta_paciente_cpf_fkey" FOREIGN KEY ("paciente_cpf") REFERENCES "Paciente"("cpf") ON DELETE RESTRICT ON UPDATE CASCADE;