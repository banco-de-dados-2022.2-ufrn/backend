insert into "Categoria" ("id", "nome") values (2, 'Soro');

insert into "Categoria" ("id", "nome") values (3, 'Fezes');

insert into "Categoria" ("id", "nome") values (4, 'Sangue');

insert into "Categoria" ("id", "nome") values (5, 'Sangue');

insert into "Categoria" ("id", "nome") values (6, 'Soro');

insert into "Categoria" ("id", "nome") values (7, 'Hemograma');

insert into "Categoria" ("id", "nome") values (10, 'Sangue');

insert into "Categoria" ("id", "nome") values (11, 'Hemograma');

insert into "Categoria" ("id", "nome") values (13, 'Sangue');

insert into "Categoria" ("id", "nome") values (16, 'Soro');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (4543, 2921, 2, 'Corticoide');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (2843, 3874, 3, 'Alprazolam');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (1830, 3473, 4, 'Diclofenaco');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (7065, 6676, 5, 'Alprazolam');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (1845, 8361, 6, 'Aspirina');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (5854, 8396, 7, 'Prednisona');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (2184, 4531, 10, 'Nimesulida');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (
        2120,
        4677,
        11,
        'Dipirona Sódica'
    );

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (2944, 9031, 13, 'Ibuprofeno');

insert into
    "Remedio" (
        "dosagem_maxima",
        "dosagem_minima",
        "id",
        "nome"
    )
values (2287, 6309, 16, 'Aspirina');

insert into
    "Enfermeiro" ("coren", "nome")
values (
        '907669',
        'Mariana Nogueira Filho'
    );

insert into
    "Enfermeiro" ("coren", "nome")
values ('942593', 'Eloá Oliveira');

insert into
    "Enfermeiro" ("coren", "nome")
values ('170744', 'Danilo Reis');

insert into
    "Enfermeiro" ("coren", "nome")
values ('391812', 'Warley Moraes');

insert into
    "Enfermeiro" ("coren", "nome")
values (
        '190571',
        'Maria Júlia Oliveira'
    );

insert into
    "Enfermeiro" ("coren", "nome")
values ('822612', 'Júlia Carvalho');

insert into
    "Enfermeiro" ("coren", "nome")
values (
        '106538',
        'Maria Luiza Oliveira'
    );

insert into
    "Enfermeiro" ("coren", "nome")
values ('821018', 'Elísio Moraes');

insert into
    "Enfermeiro" ("coren", "nome")
values ('866663', 'Eloá Saraiva');

insert into
    "Enfermeiro" ("coren", "nome")
values ('880843', 'Yuri Albuquerque');

insert into
    "Medico" ("crm", "nome")
values (
        '538331',
        'Víctor Albuquerque'
    );

insert into
    "Medico" ("crm", "nome")
values ('577650', 'Karla Barros');

insert into
    "Medico" ("crm", "nome")
values ('382569', 'Washington Santos');

insert into
    "Medico" ("crm", "nome")
values ('860037', 'Breno Braga');

insert into
    "Medico" ("crm", "nome")
values ('888572', 'João Lucas Barros');

insert into
    "Medico" ("crm", "nome")
values ('175460', 'Aline Saraiva');

insert into
    "Medico" ("crm", "nome")
values ('229298', 'João Macedo');

insert into
    "Medico" ("crm", "nome")
values ('680130', 'Nicolas Martins');

insert into
    "Medico" ("crm", "nome")
values (
        '339779',
        'Felícia Xavier Filho'
    );

insert into
    "Medico" ("crm", "nome")
values ('349432', 'Emanuel Franco');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (2, 2, 'Vitamina B12');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (3, 3, 'Hemograma');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (4, 4, 'TSH');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (5, 5, 'Ferro');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (6, 6, 'Creatinina');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (7, 7, 'T3');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (10, 10, 'Ferro');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (11, 11, 'TTPA');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (13, 13, 'Creatinina');

insert into
    "Exame" ("categoria_id", "id", "nome")
values (16, 16, 'Ferro');

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.36,
        '2022-12-22 03:16:18.489',
        'Dolorum veniam laboriosam quisquam. Beatae tempora repellendus. Nemo autem similique occaecati temporibus nostrum. Animi amet beatae assumenda ab accusamus culpa beatae ex alias. Nobis incidunt accusamus.',
        '907669',
        2,
        1,
        73,
        '538331',
        '38214766704',
        148.4,
        '15.28/9.73',
        2,
        36.9
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        2.16,
        '2022-12-21 20:38:42.268',
        'Quidem voluptatum eaque. Voluptate maiores repudiandae nostrum ipsam at. Fuga architecto reiciendis incidunt fuga perspiciatis doloribus similique.',
        '942593',
        3,
        2,
        14,
        '577650',
        '28108337715',
        106.74,
        '8.46/6.88',
        3,
        35.33
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.93,
        '2022-12-22 12:48:50.136',
        'Alias laudantium est nemo labore quaerat. Beatae quo ut consectetur veniam voluptatibus nesciunt quidem. Dolores optio provident nobis. Error ullam fugiat alias consequatur veniam esse sapiente temporibus quam. Distinctio numquam voluptatum quos.',
        '170744',
        4,
        3,
        20,
        '382569',
        '75605549654',
        77.99,
        '10.29/5.81',
        4,
        39.86
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.13,
        '2022-12-22 14:33:22.918',
        'Id voluptatum nobis accusamus repellendus accusantium. Natus delectus voluptatibus deleniti nostrum corrupti quod voluptate architecto. Ipsam possimus labore quaerat saepe molestias ea illum odio dolorem.',
        '391812',
        5,
        4,
        53,
        '860037',
        '79850782101',
        117.14,
        '13.99/6.44',
        5,
        39.55
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.78,
        '2022-12-22 10:13:22.811',
        'Tempore reiciendis non porro sequi voluptatibus delectus facilis. Sequi harum laudantium quae natus. Et voluptatum libero quibusdam quibusdam. Expedita totam voluptate vitae distinctio recusandae nihil.',
        '190571',
        6,
        5,
        12,
        '888572',
        '64305404678',
        124.95,
        '15.26/10.03',
        6,
        39.87
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        2.17,
        '2022-12-21 23:17:36.54',
        'Ipsam maiores cupiditate sit magnam nisi. Expedita voluptatibus aliquam nam recusandae perferendis aliquid. Consequatur alias aliquam.',
        '822612',
        7,
        6,
        13,
        '175460',
        '52946866194',
        110.97,
        '15.29/10.93',
        7,
        39.48
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.49,
        '2022-12-22 17:07:27.453',
        'Placeat dolorum ducimus consequuntur unde voluptates voluptatem. Nemo officiis iure. Iusto hic et illum deleniti fuga.',
        '106538',
        10,
        7,
        82,
        '229298',
        '75605414001',
        125.71,
        '14.96/6.69',
        10,
        37.19
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        2.3,
        '2022-12-22 10:57:47.31',
        'Praesentium ab dolorum sed maiores aspernatur dolorem quia ratione. Saepe culpa itaque. Beatae nulla consectetur impedit in voluptas perferendis expedita magni.',
        '821018',
        11,
        8,
        95,
        '680130',
        '77665214313',
        106.61,
        '14.07/10.75',
        11,
        36.99
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        2.13,
        '2022-12-21 20:55:50.057',
        'Corporis sit eum ad. Enim natus at at consectetur. Esse voluptatibus dolorum repellat voluptatibus. Nulla laudantium autem sint asperiores.',
        '866663',
        13,
        9,
        93,
        '339779',
        '57101689512',
        34.75,
        '10.58/6.22',
        13,
        39.57
    );

insert into
    "Consulta" (
        "altura",
        "data_horario_consulta",
        "descricao",
        "enfermeiro_coren",
        "exames_solicitados_id",
        "id",
        "idade",
        "medico_crm",
        "paciente_cpf",
        "peso",
        "pressao_arterial",
        "remedio_id",
        "temperatura"
    )
values (
        1.55,
        '2022-12-22 11:39:30.574',
        'Eos molestiae consequatur hic. Velit dolorem corporis voluptate iure fugiat. Modi soluta in sequi ullam. Asperiores saepe odio facere eum iste.',
        '880843',
        16,
        10,
        52,
        '349432',
        '74970536508',
        86.16,
        '15/8.3',
        16,
        39.2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '38214766704',
        false,
        true,
        true,
        true,
        'Deneval Nogueira',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '28108337715',
        false,
        true,
        true,
        false,
        'Márcia Saraiva',
        1
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '75605549654',
        true,
        true,
        false,
        true,
        'Fabiano Macedo',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '79850782101',
        false,
        false,
        true,
        true,
        'Warley Saraiva',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '64305404678',
        true,
        true,
        false,
        true,
        'Dra. Mariana Batista',
        1
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '52946866194',
        true,
        false,
        true,
        true,
        'Antonella Oliveira',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '75605414001',
        false,
        false,
        false,
        true,
        'Ana Laura Reis',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '77665214313',
        false,
        false,
        true,
        false,
        'Ofélia Silva',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '57101689512',
        false,
        false,
        true,
        false,
        'Carlos Barros Neto',
        2
    );

insert into
    "Paciente" (
        "cpf",
        "diabetico",
        "doenca_cronica",
        "hipertenso",
        "inadimplente",
        "nome",
        "tipo_diabetes"
    )
values (
        '74970536508',
        true,
        true,
        true,
        false,
        'Paulo Nogueira Neto',
        1
    );